import {assert} from 'chai';
import app from '../app';
import {ExpressDataApplication} from '@themost/express';
describe('Translate', () => {
    /**
     * @type ExpressDataContext
     */
    let context;

    before(done => {
        context = app.get(ExpressDataApplication.name).createContext();
        return done();
    });
    it('should translate message', async () => {
        const msg1 = "Course types: this is the first message";
        const msg2  = "Course types: this is the second message";
       // assert.isFalse(context.__(msg1)===context.__(msg2));
        // remove ":" character. This is used at i18n translate function to store translated value
        console.log(context.__(msg1));
        console.log(context.__(msg2));
        console.log(context.__(msg1.replace(/:/g, ' ')));
        console.log(context.__(msg2.replace(/:/g, ' ')));

        assert.isFalse(context.__(msg1.replace(/:/g, ' '))===context.__(msg2.replace(/:/g, ' ')));
   });

});
