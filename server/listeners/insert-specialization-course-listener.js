import {DataError} from '@themost/common';
import {DataConflictError} from "../errors";
import StudyProgramCourse from '../models/study-program-course-model';
import StudyProgramSpecialty from '../models/study-program-specialty-model';
/**
 * @param {DataEventArgs} event
 */
export async function beforeSaveAsync(event) {
    if (event.state === 1) {
        // replace specialization with specialty index
        // only if SpecializationCourse model is sealed
        // this is a backward compatibility operation
        // and it's going to be removed in next versions
        if (event.model.sealed) {
            /**
             * @type {SpecializationCourse|*}
             */
            const insert = event.target;
            // get context
            const context = event.model.context;
            // get specialization
            if (insert.specialization == null) {
                throw new DataError('E_REQUIRED', 'Specialization cannot be empty', null, 'SpecializationCourse', 'specialization')
            }
            const insertSpecialization = context.model(StudyProgramSpecialty).convert(insert.specialization);
            // try to find specialty index for the given study program
            const studyProgramCourse = await context.model(StudyProgramCourse).where('id')
                .equal(insert.studyProgramCourse)
                .silent()
                .getItem();
            // validate studyProgramCourse
            if (studyProgramCourse == null) {
                throw new DataConflictError('Parent study program course cannot be found', null, 'SpecializationCourse');
            }
            // get original specialization
            const specialization = await context.model(StudyProgramSpecialty)
                .where('studyProgram').equal(studyProgramCourse.studyProgram)
                .and('id').equal(insertSpecialization.getId())
                .silent()
                .getItem();
            if (specialization === null) {
                throw new DataConflictError('Specialization cannot be found', null, 'SpecializationCourse');
            }
            // replace specialization with specialization index
            insert.specialization = specialization.specialty;
        }
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    beforeSaveAsync(event).then( () => {
        return callback();
    }).catch( err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 */
export async function afterExecuteAsync(event) {

}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterExecute(event, callback) {
    afterExecuteAsync(event).then( () => {
        return callback();
    }).catch( err => {
        return callback(err);
    });
}
