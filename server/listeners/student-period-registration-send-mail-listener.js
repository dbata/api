import {TraceUtils} from '@themost/common/utils';
import mailer from '@themost/mailer';
import moment from 'moment';
import _ from 'lodash';
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    //the listener does not return error

    let context = event.model.context;
    let previous = event.previous;

    // get previous status
    const previousStatus = previous && previous.status ? previous.status.id : null;
    try {
        // clone data
        let data = _.cloneDeep(event.target);
        if (typeof data === 'undefined' || data === null) {
            TraceUtils.error('An error occurred while sending student period registration email. Data is null or undefined');
        }
        //get current student
        return event.model.resolveMethod('student', [], function (err, student) {
            if (err) {
                TraceUtils.error('An error occurred while sending student period registration email.');
                TraceUtils.error(err);
                return callback();
            }
            // send mail only if student period registration is modified by student
            if (data.student === student) {
                // get student email
                return context.model('Student').where('id').equal(student)
                    .expand('department', 'person')
                    .silent()
                    .getItem().then((student) => {
                        data.student = student;
                        // get mail template depending on previous status and current state
                        return context.model('MailConfiguration').where('target').equal(event.model.name)
                            .and('previousStatus').equal(previousStatus)
                            .and('status').equal(data.status)
                            .and('state').equal(event.state)
                            .silent()
                            .getItem().then((mailTemplate) => {
                                if (typeof mailTemplate === 'undefined' || mailTemplate === null) {
                                    return callback();
                                }
                                if (typeof mailTemplate.template === 'string') {
                                    if (student.person.email == null || (student.person.email && typeof student.person.email !== 'string')) {
                                        TraceUtils.warn(`Cannot send email for ${student.person.familyName} ${student.person.givenName}. Student [${student.id}] email does not exist.`);
                                        return callback();
                                    }
                                    // get only successfully registered classes
                                    data.classes = data.classes.filter(function (x) {
                                        if (typeof x.validationResult === 'undefined')
                                            return false;
                                        return x.validationResult.success && (x.validationResult.code === 'SUCC' || x.validationResult.code === 'UPD');
                                    });
                                    // send mail
                                    return mailer.getMailer(context)
                                        .to(student.person.email)
                                        .subject(mailTemplate.subject)
                                        .bcc(mailTemplate.bcc || '')
                                        .template(mailTemplate.template).send(Object.assign(data, {
                                            html: {
                                                moment: moment
                                            }
                                        }), (err) => {
                                            if (err) {
                                                TraceUtils.error(`An error occurred while sending student period registration message: ${student.person.familyName} ${student.person.givenName}. Student [${student.id}]`);
                                                TraceUtils.error(err);
                                            }
                                            return callback();
                                        });
                                }
                                return callback();
                            });
                    });
            }
            return callback();
        });
    } catch (err) {
        TraceUtils.error('An error occurred while sending student period registration message:' + event.target.id);
        TraceUtils.error(err);
        return callback();
    }
}
