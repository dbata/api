import {PercentileRanking} from "@universis/percentile-ranking";
import {HttpServerError, TraceUtils} from "@themost/common";

/**
 *
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event){
    if (["closed", "completed"].includes(event?.target?.status?.alternateName) && !["closed", "completed"].includes(event?.previous?.status?.alternateName) ){
        const context = event?.model?.context;
        const exam = await context.model('CourseExam').where('id').equal(event?.target?.id).expand({
            "name": "course",
            "options":
                {
                    "$expand": "department($expand=organization($expand=instituteConfiguration))"
                }
        }).getTypedItem();
        const students = await (await exam.getStudents()).getItems();
        let studentClasses = await context.model('StudentCourseClasses')
            .where('student').in(students.map(x=> x.student))
            .and('courseClass').in(students.map(x=> x.courseClass))
            .and('finalGrade').notEqual(null)
            .getItems();
        const examStudents = [...studentClasses];
        let instituteConfiguration = exam?.course?.department?.organization?.instituteConfiguration;
        if(Array.isArray(studentClasses) && studentClasses.length < instituteConfiguration?.percentileRankItemThreshold) {
            let courseClasses = await context.model('CourseClass')
                .where('course').equal(exam?.course)
                .orderByDescending('year').thenByDescending('period')
                .getTypedItems();
            for (const courseClass of courseClasses) {
                const classes = await context.model('StudentCourseClasses')
                    .where('courseClass').equal(courseClass)
                    .and('finalGrade').notEqual(null)
                    .getItems();
                studentClasses = [...studentClasses, ...classes];
                if(studentClasses.length >= instituteConfiguration?.percentileRankItemThreshold) {
                    break;
                }
            }
        }
        // get unique student course class objects
        studentClasses = studentClasses.filter((value, index, self) => self.findIndex((m) => m.id === value.id) === index);
        let grades = studentClasses.map(x => {return {
            grade: x.finalGrade, student: x.student, percentileRank: x.percentileRank
        }});

        switch (instituteConfiguration?.percentileRankMethod) {
            case 1:
                PercentileRanking.simplePercentileCalculation(grades);
                break;
            case 2:
                PercentileRanking.complexPercentileCalculation(grades);
                break;
            default:
                throw new HttpServerError("Percentile rank method not implemented yet.", "The percentile rank your institute has selected is not yet implemented.");
        }
        let res = [];
        const filteredStudents = grades.filter(x=> examStudents.map(student=> student.student).includes(x.student));
        for (const item of filteredStudents){
            let {grade , ...y} = item;
            let cc = studentClasses.find(x=> x.student === item.student)
            res.push({...cc,...y});
        }
        try{
            return context.model('StudentCourseClass').silent().save(res);
        } catch (e) {
            TraceUtils.error(e);
        }
    }

}

export function afterSave(event, callback){
    afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
