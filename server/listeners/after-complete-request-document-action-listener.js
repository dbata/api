import ActionStatusType from '../models/action-status-type-model';
import {DataError, TraceUtils} from "@themost/common";

/**
 * @async
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    if (event.state !== 2) {
        return;
    }
    const context = event.model.context;
    const target = event.model.convert(event.target);
    // get action status
    const actionStatus = await target.property('actionStatus').getItem();
    // if action status is other that active
    if (actionStatus.alternateName !== ActionStatusType.CompletedActionStatus) {
        // exit
        return;
    }
    // get previous action status
    const previousActionStatus = event.previous && event.previous.actionStatus;
    if (previousActionStatus === null) {
        throw new Error('Previous status cannot be empty at this context.');
    }
    // validate previous status
    if (previousActionStatus.alternateName !== 'ActiveActionStatus') {
       return;
    }
    // get request
    const request = await context.model(event.model.name).where('id').equal(event.target.id).silent().getItem();
    // get student
    const student = context.model('Student').convert(request.student);
     // get document configuration
    const config = await context.model('DocumentConfiguration')
        .find(request.object)
        .expand('reportTemplate')
        .silent()
        .getItem();

    if (config.reportTemplate) {
        // should print document and relate it to request
        /**
         * @type {ReportTemplate|*}
         */

        // get DepartmentDocumentNumberSeries
        let department = student.department;
        if (department == null) {
            department = await context.model('Student').select('department').where('id').equal(student.id).silent().value();
        }
        const documentNumberSerie = await context.model('DepartmentDocumentNumberSeries')
            .where('department').equal(department)
            .and('active').equal(1)
            .and('isDefault').equal(1)
            .silent().getItem();
        if (documentNumberSerie == null) {
            throw new Error(`Document number series is not set for department ${department}`);
        }
        const reportTemplate = context.model('ReportTemplate').convert(config.reportTemplate);
        // get report
        const printData = {
            "ID": student.id,
            "REPORT_USE_DOCUMENT_NUMBER": true,
            "REPORT_DOCUMENT_SERIES": documentNumberSerie.id
        };
        // print report
        try {
            const file = await reportTemplate.print(printData);
            if (file) {
                /**
                 * @type {DocumentNumberSeriesItem|*}
                 */
                const result = await context.model('DocumentNumberSeriesItem').where('url').equal(file.contentLocation).select('id').silent().value();
                // save result to request
                target.result = result;
                await event.model.silent().save(target);
                // check if report requires digital signature
                // if (!reportTemplate.signReport) {
                //     const seriesItem = {
                //         "id":result,
                //         "published":true,
                //         "datePublished":new Date()
                //     };
                //     // save result
                //     await context.model('DocumentNumberSeriesItem').save(seriesItem);
                // }
            } else {
                throw new DataError(`Cannot print report ${reportTemplate.name} for student with id ${student.id}`);
            }
        } catch (err) {
            TraceUtils.error(err);
            throw new DataError(`Cannot print report ${reportTemplate.name} for student with id ${student.id}`);
        }
    }
}


export function beforeSave(event, callback) {
    return callback();
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    // execute async method
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
