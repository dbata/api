import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import CourseClass = require('./course-class-model');
import CourseExam = require('./course-exam-model');

/**
 * @class
 */
declare class CourseExamClass extends DataObject {

     
     /**
      * @description Ο μοναδικός κωδικός της σύνδεσης τάξης - εξέτασης
      */
     public id: string; 
     
     /**
      * @description Κωδικός τάξης
      */
     public courseClass: CourseClass|any; 
     
     /**
      * @description Κωδικός Εξέτασης
      */
     public courseExam: CourseExam|any; 
     
     /**
      * @description Ημερομηνία τελευταίας τροποποίησης
      */
     public dateModified: Date; 

}

export = CourseExamClass;