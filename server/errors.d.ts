import { DataError, HttpError } from "@themost/common";

export declare class RegistrationError extends HttpError {
    constructor(message?: string, innerMessage?: string);
}

export declare class ProgramCourseNotFound extends HttpError {
    constructor(message?: string, innerMessage?: string);
}

export declare class StudentNotFound extends HttpError {
    constructor(message?: string, innerMessage?: string);
}


export declare class GenericDataConflictError extends DataError {
    constructor(code: string, message?: string, innerMessage?: string, model?: string);
}

export declare class DataConflictError extends GenericDataConflictError {
    constructor(message?: string, innerMessage?: string, model?: string);
}


export declare class ValidationResult {
    constructor(success: boolean, code?: string, message?: string, innerMessage?: string, data?: any);
    success: boolean;
    code?: string;
    message?: string;
    innerMessage?: string;
    data?: any
}
