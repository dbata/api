import app from '../server/app';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../server/utils';
import ReportTemplate from '../server/models/report-template-model';
import ReportCategory from '../server/models/report-category-model';
import {PrintService} from "@universis/reports";
const executeInTransaction = TestUtils.executeInTransaction;

describe('ReportTemplate', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        process.env.NODE_ENV = 'test';
        return done();
    });
    afterAll(done => {
        if (context) {
            return context.finalize(() => done());
        }
        process.env.NODE_ENV = 'development';
        return done();
    });
    beforeEach( done => {
        // clear configuration cache
        const configuration = app.get(ExpressDataApplication.name).getConfiguration();
        configuration.cache = {};
        context = app.get(ExpressDataApplication.name).createContext();
        return done();
    });
   it('should get report template', async() => {
       await executeInTransaction(context, async () => {
           const items = await context.model(ReportTemplate).silent().getItems();
           expect(items).toBeInstanceOf(Array);
       });
   });
    it('should add report template', async() => {
        await executeInTransaction(context, async () => {
            let item = {
                name: 'Simple Student Certificate',
                alternateName: 'SimpleStudentCertificate1',
                url: '/reports/universis/Blank_A4'
            };
            await context.model(ReportTemplate).silent().save(item);
            item = await context.model(ReportTemplate).where('id').equal(item.id).silent().getItem();
            expect(item).toBeTruthy();
            expect(item.alternateName).toBe('SimpleStudentCertificate1');
        });
    });
    it('should remove report template', async() => {
        await executeInTransaction(context, async () => {
            let item = {
                name: 'Simple Student Certificate',
                alternateName: 'SimpleStudentCertificate1',
                url: '/reports/universis/Blank_A4'
            };
            await context.model(ReportTemplate).silent().save(item);
            item = await context.model(ReportTemplate).where('id').equal(item.id).silent().getItem();
            await context.model(ReportTemplate).silent().remove(item);
            item = await context.model(ReportTemplate).where('id').equal(item.id).silent().getItem();
            expect(item).toBeFalsy();
        });
    });
    it('should add report category', async() => {
        await executeInTransaction(context, async () => {
            let item = {
                name: 'Student Reports',
                alternateName: 'StudentReports'
            };
            await context.model(ReportCategory).silent().save(item);
            item = await context.model(ReportCategory).where('id').equal(item.id).silent().getItem();
            expect(item).toBeTruthy();
            expect(item.alternateName).toBe('StudentReports');
        });
    });
    it('should remove report category', async() => {
        await executeInTransaction(context, async () => {
            let item = {
                name: 'Student Reports',
                alternateName: 'StudentReports',
                appliesTo: 'Student'
            };
            await context.model(ReportCategory).silent().save(item);
            item = await context.model(ReportCategory).where('id').equal(item.id).silent().getItem();
            await context.model(ReportCategory).silent().remove(item);
            item = await context.model(ReportCategory).where('id').equal(item.id).silent().getItem();
            expect(item).toBeFalsy();
        });
    });
    it('should get server available reports', async() => {
        await executeInTransaction(context, async () => {
            // get a user which belongs to admins
            let user = await context.model('User').where('groups/name').equal('Administrators').silent().getItem();
            expect(user).toBeTruthy('Expected a user with admin privileges');
            // set context user
            context.user = {
                name: user.name
            };
            const serverTemplates = await ReportTemplate.getAvailableServerTemplates(context);
            expect(serverTemplates).toBeInstanceOf(Array);
            // reset context user
            context.user = null;
        });
    });
    it('should deny access for server templates', async() => {
        await executeInTransaction(context, async () => {
            // get a user which belongs to admins
            let user = await context.model('User').where('groups/name').equal('Students').silent().getItem();
            expect(user).toBeTruthy('Expected a student user');
            // set context user
            context.user = {
                name: user.name
            };
            await expectAsync((function() {
                return ReportTemplate.getAvailableServerTemplates(context);
            })()).toBeRejected();
            // reset context user
            context.user = null;
        });
    });
    it('should print simple report', async() => {
        await executeInTransaction(context, async () => {
            // get a student
            let student = await context.model('Student').where('studentStatus/alternateName').equal('active').silent().getItem();
            expect(student).toBeTruthy('Expected a student user');
            // get a user which belongs to admins
            let user = await context.model('User').where('groups/name').equal('Administrators').silent().getItem();
            expect(user).toBeTruthy('Expected a user with admin privileges');
            // set context user
            context.user = {
                name: user.name
            };
            /**
             * @type {PrintService}
             */
            const printService = context.getApplication().getService(PrintService);
            const serverTemplates = await ReportTemplate.getAvailableServerTemplates(context);
            // expected a valid server report with label Blank_A4
            const findServerReport = serverTemplates.find( x => {
                return x.label === 'Blank_A4';
            });
            expect(findServerReport).toBeTruthy();
            const buffer = await printService.print(findServerReport.uri, '.pdf', {
                ID: student.id,
                REPORT_CLIENT_TOKEN: process.env.REPORT_CLIENT_TOKEN
            });
            expect(buffer).toBeTruthy();
        });
    });
    it('should print report template', async() => {
        await executeInTransaction(context, async () => {
            // get a student
            let student = await context.model('Student').where('studentStatus/alternateName').equal('active').silent().getItem();
            expect(student).toBeTruthy('Expected a student user');
            // get a user which belongs to admins
            let user = await context.model('User').where('groups/name').equal('Administrators').silent().getItem();
            expect(user).toBeTruthy('Expected a user with admin privileges');
            // set context user
            context.user = {
                name: user.name
            };
            const serverTemplates = await ReportTemplate.getAvailableServerTemplates(context);
            // expected a valid server report with label Blank_A4
            const findServerReport = serverTemplates.find( x => {
                return x.label === 'Blank_A4';
            });
            expect(findServerReport).toBeTruthy();

            // create report category
            let reportCategory = {
                name: 'Simple Student Reports',
                alternateName: 'SimpleStudentReports',
                appliesTo: 'Student'
            };
            await context.model(ReportCategory).silent().save(reportCategory);

            // create report
            let reportTemplate = {
                name: 'Simple Student Certificate',
                alternateName: 'SimpleStudentCertificate1',
                url: findServerReport.uri,
                reportCategory: reportCategory
            };
            await context.model(ReportTemplate).silent().save(reportTemplate);

            /**
             * get report template
             * @type {ReportTemplate}
             */
            reportTemplate = await context.model(ReportTemplate).where('id').equal(reportTemplate.id).getTypedItem();
            const stream = await reportTemplate.print({
                ID: student.id
            });
            expect(stream).toBeTruthy();
        });
    });
});
