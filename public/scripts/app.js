/* eslint-disable no-var */
var app = angular.module("docs", ["ui.router"]);

app.controller('SidebarController', function($http) {
// eslint-disable-next-line no-invalid-this
    var vm = this;
    function activate() {
        $http.get('api/').then(function (response) {
            vm.endpoints = response.data.value;
        });
    }
    activate();
});

function getEntityType(schema, name) {
    var entityType = schema.find('EntityType').toArray().find(function(element) {
        return element.getAttribute('Name') === name;
    });
    if (entityType == null) {
        return;
    }

    var property = Array.from(entityType.querySelectorAll("Property")).map(function(element) {
        var type = element.getAttribute('Type');
        var propertyType = null;
        if (type.indexOf('Edm.') < 0) {
            propertyType = type;
        }
        return {
            name: element.getAttribute('Name'),
            type: type,
            nullable: element.getAttribute('Nullable'),
            propertyType: propertyType ? propertyType.replace(/Collection\((\w+)\)/, "$1") : null
        }
    });
    var navigationProperty = Array.from(entityType.querySelectorAll("NavigationProperty")).map(function(element) {
        var type = element.getAttribute('Type');
        var propertyType = null;
        if (type.indexOf('Edm.') < 0) {
            propertyType = type;
        }
        return {
            name: element.getAttribute('Name'),
            type: element.getAttribute('Type'),
            propertyType: propertyType ? propertyType.replace(/Collection\((\w+)\)/, "$1") : null
        }
    });
    property.push.apply(property, navigationProperty);

    var inheritedProperty = [];

    if (entityType.getAttribute('BaseType') !== '') {
        var baseType = getEntityType(schema, entityType.getAttribute('BaseType'));
        if (baseType) {
            inheritedProperty = baseType.property;
            if (baseType.inheritedProperty.length) {
                inheritedProperty.push.apply(inheritedProperty, baseType.inheritedProperty);
            }
        }
    }

    return {
        name: entityType.getAttribute('Name'),
        baseType: entityType.getAttribute('BaseType'),
        property: property.sort(function(a, b) {
            if (a.name > b.name) {
                return 1;
            }
            if (a.name < b.name) {
                return -1;
            }
            return 0;
        }),
        inheritedProperty: inheritedProperty.sort(function(a, b) {
            if (a.name > b.name) {
                return 1;
            }
            if (a.name < b.name) {
                return -1;
            }
            return 0;
        })
    }

}

app.controller('EntityTypesController', function($http) {
// eslint-disable-next-line no-invalid-this
    var vm = this;
    function activate() {
        $http.get('api/$metadata').then(function (response) {
            // get entity types
            var xml = jQuery(response.data);
            vm.entityTypes = xml.find('EntityType').toArray().map(function(entityType) {
                return {
                    name: entityType.getAttribute('Name'),
                    baseType: entityType.getAttribute('BaseType')
                }
            }).sort((a, b) => {
                if (a.name > b.name) {
                    return 1;
                }
                if (a.name < b.name) {
                    return -1;
                }
                return 0;
            });
        });
    }
    activate();
});

app.controller('EntityTypeController', function($http, $stateParams) {
// eslint-disable-next-line no-invalid-this
    var vm = this;
    function activate() {
        $http.get('api/$metadata').then(function (response) {
            // get entity types
            var schema = jQuery(response.data);
            vm.entityType = getEntityType(schema, $stateParams.name);
        });
    }
    activate();
});

app.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider.state({
        name: 'index',
        url: '/',
        templateUrl: 'components/index.component.html'
    }).state({
        name: 'entityTypes',
        url: '/entityTypes',
        templateUrl: 'components/entity-types.component.html'
    }).state({
        name: 'entityType',
        url: '/entityTypes/:name',
        templateUrl: 'components/entity-type.component.html'
    });

    // noinspection JSCheckFunctionSignatures
    $urlRouterProvider.otherwise('/');
});
